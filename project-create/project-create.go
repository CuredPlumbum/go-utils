package main

import (
	"log"
	"os"
	"path"
	"strings"
	"text/template"
)

var dotProject = `<?xml version="1.0" encoding="UTF-8"?>
<projectDescription>
	<name>z-{{.}}</name>
	<comment></comment>
	<projects>
	</projects>
	<buildSpec>
	</buildSpec>
	<natures>
	</natures>
</projectDescription>
`

var dotProjectFilename = ".project"

func main() {
	err := m()
	if err != nil {
		log.Fatalln(err)
	}
	log.Println("Done!")
}

func m() error {
	tmpl, err := template.New(dotProjectFilename).Parse(dotProject)
	if err != nil {
		return err
	}

	wd, err := os.Getwd()
	if err != nil {
		return err
	}
	_, wd = path.Split(strings.Replace(wd, `\`, `/`, -1))

	f, err := os.Create(dotProjectFilename)
	if err != nil {
		return err
	}
	defer f.Close()

	return tmpl.Execute(f, wd)

}
