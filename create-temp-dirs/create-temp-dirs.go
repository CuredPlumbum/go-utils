package main

import (
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
	"time"
)

const (
	isoDateFormat  = "2006-01-02"
	isoMonthFormat = "2006-01"
)

var keepAliveDays = flag.Int("n", 14, "Days number to store folders")
var dir string

func createDir(dir string, subdir string) {
	dirToCreate := filepath.Clean(filepath.Join(dir, subdir))

	fmt.Println("Creating", dirToCreate)

	if err := os.MkdirAll(dirToCreate, 0755); err != nil {
		fmt.Fprintln(os.Stderr, "Can not create directory "+dirToCreate+". Reason is", err)
	} else {
		fmt.Println("Directory", dirToCreate, "successfully created")
	}

}

func main() {

	flag.Parse() // Scan the arguments list

	if len(flag.Args()) < 1 {
		flag.Usage()
		os.Exit(1)
	}

	dir = flag.Arg(0)

	fmt.Println("Working directory is:", dir)
	fmt.Println("Days to keep:", *keepAliveDays)

	now := time.Now()
	createDir(dir, now.Format(isoDateFormat))

	edgeDate := time.Now().AddDate(0, 0, -*keepAliveDays)
	edgeDate = time.Date(edgeDate.Year(), edgeDate.Month(), edgeDate.Day(), 0, 0, 0, 0, edgeDate.Location()) //round

	fmt.Println("Collecting directory older than", edgeDate.Format(isoDateFormat), "...")
	defer fmt.Println("Done.")
	files, err := ioutil.ReadDir(dir)
	if err != nil {
		log.Fatal("Can not read direcotory", dir, err)
	}

	for _, file := range files {
		if !file.IsDir() {
			continue
		}
		if t, err := time.Parse(isoDateFormat, file.Name()); err == nil {
			if !t.Before(edgeDate) {
				continue
			}

			createDir(dir, t.Format(isoMonthFormat))
			monthDir := filepath.Join(dir, t.Format(isoMonthFormat))

			oldDir := filepath.Join(dir, file.Name())
			newDir := filepath.Join(monthDir, file.Name())

			if err := os.Rename(oldDir, newDir); err != nil {
				fmt.Fprintln(os.Stderr, "Can not move directory "+oldDir+" to "+newDir+". Reason is", err)
			} else {
				fmt.Println(oldDir, "successfully moved to", newDir)
			}
		}
	}
}
